package su.nsk.iae.edtl.edtl_to_ltl

import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.xtext.generator.IFileSystemAccess2
import org.eclipse.xtext.generator.IGeneratorContext
import su.nsk.iae.edtl.generator.IEdtlGenerator
import su.nsk.iae.edtl.edtl.Model
import su.nsk.iae.edtl.edtl.Expression
import java.util.ArrayList
import su.nsk.iae.edtl.edtl.PrimaryExpression

import static extension org.eclipse.emf.ecore.util.EcoreUtil.*
import static extension org.eclipse.xtext.EcoreUtil2.*
import su.nsk.iae.edtl.edtl.AndExpression
import su.nsk.iae.edtl.edtl.XorExpression
import su.nsk.iae.edtl.edtl.impl.AndExpressionImpl
import su.nsk.iae.edtl.edtl.impl.XorExpressionImpl
import su.nsk.iae.edtl.edtl.CompExpression
import su.nsk.iae.edtl.edtl.Requirement
import su.nsk.iae.edtl.edtl.UnExpression
import com.opencsv.CSVWriter
import java.io.File
import java.io.FileWriter

class LtlGenerator implements IEdtlGenerator {
	
	static final String filepath = "/Users/diskursmonger/edtl_project/su.nsk.iae.edtl.edtl_to_ltl/output.csv"
	
//	static final String filepath = System.getProperty("user.dir")
	
	val headerCsv = newArrayList(" ", "req name", "trigger", "invariant", "final", "delay", "reaction", "release", "LTL formula")
	
	override beforeGenerate(Resource input, IFileSystemAccess2 fsa, IGeneratorContext context) {
	
		var file = new File(filepath)
//		var file = new File(System.getProperty("user.dir") + "out.csv")
		
		var outCsv = new FileWriter(file)
		var csvWriter = new CSVWriter(outCsv)
		
		csvWriter.writeNext(headerCsv)
	
		val ast = input.allContents.
					toIterable.
					filter(Model).
					get(0)
		val reqs = ast.reqs
		
		var reqNum = 1
		
		for (req : reqs) {
			var Term trigger
			var Term invariant
			var Term fin
			var Term delay
			var Term reaction
			var Term release
			
			
			if (req.trigExpr === null) {
				trigger = new BoolTerm(null, true);
			} else {
				trigger = new Term(req.trigExpr)
			}
			
			if (req.invExpr === null) {
				invariant = new BoolTerm(null, true);
			} else {
				invariant = new Term(req.invExpr)
			}
			
			if (req.finalExpr === null) {
				fin = new BoolTerm(null, false);
			} else {
				fin = new Term(req.finalExpr)
			}
			
			if (req.delayExpr === null) {
				delay = new BoolTerm(null, false);
			} else {
				delay = new Term(req.delayExpr)
			}
			
			if (req.reacExpr === null) {
				reaction = new BoolTerm(null, true);
			} else {
				reaction = new Term(req.reacExpr)
			}
			
			if (req.relExpr === null) {
				release = new BoolTerm(null, false);
			} else {
				release = new Term(req.relExpr)
			}

			val x0 = con(trigger, no(release))
			val x1 = con(invariant, reaction)
			val x2 = dis(release, x1)
			val x3 = con(invariant, no(delay))
			val x4 = until(x3, x2)
			val x5 = con(fin, x4)
			val x6 = dis(release, x5)
			val x7 = con(invariant, no(fin))
			val x8 = until(x7, x6)
			val x9 = impl(x0, x8)
			val ltl_formula = globally(x9)

			
			var out = "\nRequirement " + req.name + ":\n" + 
			"trigger: " + convertExprToString(req.trigExpr) + "\n" +
			"invariant: " + convertExprToString(req.invExpr) + "\n" +
			"final: " + convertExprToString(req.finalExpr) + "\n" +
			"delay: " + convertExprToString(req.delayExpr) + "\n" +
			"reaction: " + convertExprToString(req.reacExpr) + "\n" +
			"release: " + convertExprToString(req.relExpr) + "\n\n" +
			"LTL formula:\n" + convertTermToString(ltl_formula) + "\n"
			
			System.out.println(out)
			
			val csvRow = newArrayList(reqNum.toString(), req.name, 
				convertExprToString(req.trigExpr),
				convertExprToString(req.invExpr),
				convertExprToString(req.finalExpr),
				convertExprToString(req.delayExpr),
				convertExprToString(req.reacExpr),
				convertExprToString(req.relExpr),
				convertTermToString(ltl_formula)
			)
			
			csvWriter.writeNext(csvRow)
			reqNum++
		}
		
		csvWriter.close();
		
		
	}
	
	private def String convertAttrToString(Term attr, String str) {
		if (attr instanceof BoolTerm) {
			return attr.expr.toString
		} else {
			var attr_expr = attr.expr
			if (attr_expr instanceof PrimaryExpression) {
				if ("FALSE".equals(attr_expr.constant)) {
					return "false"
				}
				if ("TRUE".equals(attr_expr.constant)) {
					return "true"
				}
			}
			return str
		}
	}
	
//	private def void printTermArray(Term term) {
//		
//	}
	
	private def String convertTermToString(Term term) {
		if (term instanceof BoolTerm) {
			return String.valueOf(term.expr)
		}
		
		if (term instanceof VarTerm) {
			return term.expr.v.name
		}
		
		if (term instanceof NotTerm) {
			return "¬" + convertTermToString(term.expr)
		}
		
		if (term instanceof AndTerm) {
			return "(" + convertTermToString(term.left) + " ∧ " + convertTermToString(term.right) + ")"
		}
		
		if (term instanceof OrTerm) {
			return "(" + convertTermToString(term.left) + " ∨ " + convertTermToString(term.right) + ")"
		}
		
		if (term instanceof ImplTerm) {
			return "(" + convertTermToString(term.left) + " → " + convertTermToString(term.right) + ")"
		}
		
		if (term instanceof WTerm) {
			return "W(" + convertTermToString(term.expr) + ")"
		}
		
		if (term instanceof FTerm) {
			return "F(" + convertTermToString(term.left) + ")"
		}
		
		if (term instanceof GTerm) {
			return "G(" + convertTermToString(term.left) + ")"
		}
		
		if (term instanceof UTerm) {
			return "(" + convertTermToString(term.left) + " U " + convertTermToString(term.right) + ")"
		}
	
		return convertExprToString(term.expr)
	}
	
	private def String convertExprToString(Expression expr) {
		if (expr instanceof PrimaryExpression) {
			if ("FALSE".equals(expr.constant)) {
				return "false"
			}
			if ("TRUE".equals(expr.constant)) {
				return "true"
			}
			return expr.v.name
		}
		
		if (expr instanceof AndExpression) {
			return "(" + convertExprToString(expr.left) + " ∧ " + convertExprToString(expr.right) + ")"
		}
		
		if (expr instanceof UnExpression) {
			if (expr.unOp === "NOT") {
				return "¬" + convertExprToString(expr.right)
			}
			if (expr.unOp === "FE") {
				return "FE (" + convertExprToString(expr.right) + ")"
			}
			if (expr.unOp === "RE") {
				return "RE (" + convertExprToString(expr.right) + ")"
			}
			if (expr.unOp === "HIGH") {
				return "HIGH (" + convertExprToString(expr.right) + ")"
			}
			if (expr.unOp === "LOW") {
				return "LOW (" + convertExprToString(expr.right) + ")"
			}
		}
		
		if (expr.orOp !== null) {
			return "(" + convertExprToString(expr.left) + " ∨ " + convertExprToString(expr.right) + ")"
		}
		
	}
	
	private def Term con(Term left, Term right) {
		var left_expr = left.expr
		if (left_expr instanceof PrimaryExpression) {
			if ("FALSE".equals(left_expr.constant)) {
				return new BoolTerm(left_expr)
			}
		}
		
		var right_expr = right.expr
		if (right_expr instanceof PrimaryExpression) {
			if ("FALSE".equals(right_expr.constant)) {
				return new BoolTerm(right_expr)
			}
		}
		
		if (left_expr instanceof PrimaryExpression) {
			if ("TRUE".equals(left_expr.constant)) {
				return right
			}
		}
		
		if (right_expr instanceof PrimaryExpression) {
			if ("TRUE".equals(right_expr.constant)) {
				return left
			}
		}
		
		if (left instanceof BoolTerm) {
			if (!left.expr) {
				return left
			} else {
				return right
			}
		}
		
		if (right instanceof BoolTerm) {
			if (!right.expr) {
				return right
			} else {
				return left
			}
		}
		
		return new AndTerm(left, right)
	}
	
	private def Term dis(Term left, Term right) {
		if (left instanceof BoolTerm) {
			if (left.expr) {
				return left
			} else {
				return right
			}
		}
		
		if (right instanceof BoolTerm) {
			if (right.expr) {
				return right
			} else {
				return left
			}
		}
		
		var left_expr = left.expr
		if (left_expr instanceof PrimaryExpression) {
			if ("TRUE".equals(left_expr.constant)) {
				return new BoolTerm(left_expr)
			}
		}
		
		var right_expr = right.expr
		if (right_expr instanceof PrimaryExpression) {
			if ("TRUE".equals(right_expr.constant)) {
				return new BoolTerm(right_expr)
			}
		}
		
		if (left_expr instanceof PrimaryExpression) {
			if ("FALSE".equals(left_expr.constant)) {
				return right
			}
		}
		
		if (right_expr instanceof PrimaryExpression) {
			if ("FALSE".equals(right_expr.constant)) {
				return left
			}
		}
		
		// a ∨ a = a
		if (left.equals(right)) { //rewrite
			return left
		}
		
		// a ∨ (b ∧ (a ∨ c)) = a ∨ (b ∧ c)
		if (right instanceof AndTerm) {
			var right_r = right.right
			if (right_r instanceof OrTerm) {
				if (right_r.left.equals(left)) {
					return new OrTerm(left, new AndTerm(right.left, right_r.right))
				}
			}
		}
		
		// a ∨ F(a ∨ b) = F(a ∨ b)
		if (right instanceof FTerm) {
			var right_l = right.left
			if (right_l instanceof OrTerm) {
				if (left.equals(right_l.left)) {
					return right
				}
			}
		}
		
		// a ∨ (b U a) = (b U a)
		if (right instanceof UTerm) {
			var right_r = right.right
			if (right_r.equals(left)) {
				return right
			}
		}
		
		// G(¬a) ∨ F(a) = true
		if (left instanceof GTerm) {
			var left_l = left.left
			if (left_l instanceof NotTerm) {
				if (right instanceof FTerm) {
					if (left_l.expr.equals(right.left.expr)) {
						return new BoolTerm(null, true)
					}
				}
			}
		}
		
		// a ∨ F(a) = F(a)
		if (right instanceof FTerm) {
			if (right.left.equals(left)) {
				return right
			}
		}
		
		// a ∨ (a ∨ b) = a V b | a ∨ (b ∨ a) = a ∨ b
		if (right instanceof OrTerm) {
			if (right.left.equals(left) || right.right.equals(left)) {
				return right
			}
		}
		
		// #G(¬a) ∨ (F(a) ∨ (с)) = (с)
		if (left instanceof GTerm) {
			if (left.left instanceof NotTerm) {
				if (right instanceof OrTerm) {
					if (right.left instanceof FTerm) {
						if (right.left.expr.equals(left.left.expr)) {
							return right.right
						}
					}
				}
			}
		}
		
		// a ∨ (b U (a ∨ c)) = (b U (a ∨ c))
		
		if (right instanceof UTerm) {
			var right_r = right.right
			if (right_r instanceof OrTerm) {
				if (right_r.left.equals(left)) {
					return right
				}
			}
		}
		
		return new OrTerm(left, right)
	}
	
	private def checkNotHelper(Term left, Term right) {
		if (left instanceof BoolTerm && right instanceof BoolTerm) {
			return left.expr != right.expr
		}
		
		if (left instanceof NotTerm && !(right instanceof NotTerm)) {
			return left.expr.equals(right.expr)
		}
		
		if (!(left instanceof NotTerm) && right instanceof NotTerm) {
			return right.expr.equals(left.expr)
		}
		
		return false
	}
	
	private def Term until(Term left, Term right) {
		if (right instanceof BoolTerm) {
			if (right.expr) {
				return new BoolTerm(null, true)
			} else {
				return new BoolTerm(null, false)
			}
		}
		
		if (left instanceof BoolTerm) {
			if (left.expr) {
				if (!(right instanceof BoolTerm)) {
					return future(right)
				}
			} else {
				if (!(right instanceof BoolTerm)) {
					return right
				}
			}
		}
		
		// a U a = a
		if (left.equals(right)) {
			return left
		}
		
		// ¬a U a = F(a)
		if (checkNotHelper(left, right)) {
			return future(right)
		}
		
		// ¬a U (b ∨ a) = F(a) ∨ (¬a U b)
		if (left instanceof NotTerm) {
			if (right instanceof OrTerm) {
				if (checkNotHelper(left, right.right)) {
					return dis(future(left.expr), until(left, right.left))
				}
			}
		}
		
		// (a ∧ b) U (c ∨ a) = a ∨ ((a ∧ b) U c)
		if (left instanceof AndTerm) {
			if (right instanceof OrTerm) {
				if (left.left.equals(right.right)) {
					return dis(left.left, new UTerm(left, right.left))
				}
			}
		}
		
		// (a ∧ b) U a = a
		if (left instanceof AndTerm) {
			if (left.left.equals(right)) {
				return right
			}
		}
		
		// a U (b ∨ a) = a ∨ (a U b)
		if (right instanceof OrTerm) {
			if (left.equals(right.right)) {
				return dis(left, until(left, right.left))
			}
		}
		
		// (a ∧ ¬b) U (b ∧ a) = a U (b ∧ a)
		if (left instanceof AndTerm) {
			if (right instanceof AndTerm) {
				if (left.left.equals(right.right)) {
					if (checkNotHelper(left.right, right.left)) {
						return until(left.left, right)
					}
				}
			}
		}
		
		return new UTerm(left, right)
		
	}
	
	private def Term impl(Term left, Term right) {
		if (left instanceof BoolTerm || right instanceof BoolTerm) {
			return dis(no(left), right)
		} else {
			return new ImplTerm(left, right)
		}
	}
	
	private def Term future(Term t) {
		if (t instanceof BoolTerm) {
			return t
		} else {
			return new FTerm(t)
		}
	}
	
	private def Term no(Term e) {
		if (e instanceof NotTerm) {
			return e.expr
		}
		
		if (e instanceof BoolTerm) {
			e.expr = !e.expr
			return e
		}
		
		return new NotTerm(e)
	}
	
	
	
	
	private def Term globally(Term t) {
		if (t instanceof BoolTerm) {
			return t
		}
		
		// G(F(a)) = GF(a)
		if (t instanceof FTerm) {
			return new GTerm(t)
		}
		
		// G(G(a)) = G(a)
		if (t instanceof GTerm) {
			return new GTerm(t.left)
		}
		
		// G(G(a ∧ ¬b) ∨ (a U (b ∧ a))) = G(a)
		if (t instanceof OrTerm) {
			if (t.left instanceof GTerm) {
				if (t.right instanceof UTerm) {
					if (t.right.right instanceof AndTerm) {
						if (t.left.left instanceof AndTerm) {
							if (t.left.left.left.left.equals(t.right.left)) {
								if (t.right.left.equals(t.right.right.right)) {
									if (checkNotHelper(t.left.left.right, t.right.right.left)) {
										return new GTerm(t.left.left.left)
									}
								}
							}
						}
					}
				}
			}
		}
		
		// G(G(a ∧ ¬b) ∨ ((a ∧ ¬b) U (b ∧ (a U (a ∧ c))))) = G(a ∧ (G(¬b) ∨ F(b ∧ F(c))))
		if (t instanceof OrTerm) {
			if (t.left instanceof GTerm) {
				if (t.right instanceof UTerm) {
					if (t.right.right instanceof AndTerm) {
						if (t.right.right.right instanceof UTerm) {
							if (t.right.right.right.right instanceof AndTerm) {
								if (t.left.left.equals(t.right.left)) {
									if (t.left.left.left.equals(t.right.right.right.left)) {
										if (t.left.left.left.equals(t.right.right.right.right.left)) {
											if (checkNotHelper(t.left.left.right, t.right.right.left)) {
												return globally(con(t.left.left.left, dis(globally(t.left.left.right), 
													future(con(t.right.right.left, future(t.right.right.right.right.right)))
												)))
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		// G(G(a ∧ ¬b) ∨ ((a ∧ ¬b) U (b ∧ (a ∧ c)))) = G(a ∧ (G(¬b) ∨ F(b ∧ c)))
		if (t instanceof OrTerm) {
			if (t.right instanceof UTerm) {
				if (t.left instanceof GTerm) {
					if (t.left.left instanceof AndTerm) {
						if (t.right.left instanceof AndTerm) {
							if (t.left.left.equals(t.right.left)) {
								if (t.right.right instanceof AndTerm) {
									if (t.right.right.right instanceof AndTerm) {
										if (checkNotHelper(t.left.left.right, t.right.right.left)) {
											return globally(con(t.left.left.left, dis(globally(t.left.left.right), 
												future(con(t.right.right.left, t.right.right.right.right)))))
										}
									}
								}
							}
						}
					}
				}
			}
		}
		
		// G(G(a) ∨ (a U b)) = G(a ∧ F(b))
		if (t instanceof OrTerm) {
			if (t.left instanceof GTerm) {
				if (t.right instanceof UTerm) {
					if (t.left.left.equals(t.right.left)) {
						return globally(con(t.left.left, until(t.left.right, t.right.right)))
					}
				}
			}
		}
		
		return new GTerm(t)
	}
	
//	private def <T> T getInstance(Class<T> clazz) {
//		val defaultConstructor = clazz.getDeclaredConstructor()
//		defaultConstructor.setAccessible(true)
//		return defaultConstructor.newInstance()
//	}
	
	override setModel(Model model) {

	}
	
	override doGenerate(Resource input, IFileSystemAccess2 fsa, IGeneratorContext context) {

	}
	
	override afterGenerate(Resource input, IFileSystemAccess2 fsa, IGeneratorContext context) {
	
	}
}

class Term {
	public Expression expr
	public Term left
	public Term right
	
	
	new (Expression e) {
		this.expr = e
	}
}

class BoolTerm extends Term {
	public boolean expr
	
	new (PrimaryExpression e) {
		super(e)
		expr = "TRUE".equals(e.constant)
	}
	
	new (PrimaryExpression e, boolean b) {
		super(e)
		expr = b
	}
}

class VarTerm extends Term {
	public PrimaryExpression expr
	
	new (PrimaryExpression e) {
		super(e)
		expr = e
	}
}

class AndTerm extends Term {
	public Term left
	public Term right
	
	new (Term l, Term r) {
		super(l.expr)
		left = l
		right = r
	}
}

class OrTerm extends Term {
	public Term left
	public Term right
	
	new (Term l, Term r) {
		super(l.expr)
		left = l
		right = r
	}
}

class ImplTerm extends Term {
	public Term left
	public Term right
	
	new (Term l, Term r) {
		super(l.expr)
		left = l
		right = r
	}
}


class NotTerm extends Term {
	public Term expr
	
	new (Term e) {
		super(e.expr)
		expr = e
	}
}

class WTerm extends Term {
	public Term expr
	
	new (Term e) {
		super(e.expr)
		expr = e
	}
}

class UTerm extends Term {
	public Term left
	public Term right
	
	new (Term l, Term r) {
		super(l.expr)
		left = l
		right = r
	}
}


class GTerm extends Term {
	public Term left
//	public Term right
	
	new (Term l) {
		super(l.expr)
		left = l
//		right = r
	}
}

class FTerm extends Term {
	public Term left
	
	new (Term l) {
		super(l.expr)
		left = l
	}
}