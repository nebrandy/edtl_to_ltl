package su.nsk.iae.edtl.edtl_to_ltl;

@SuppressWarnings("all")
public class UTerm extends Term {
  public Term left;
  
  public Term right;
  
  public UTerm(final Term l, final Term r) {
    super(l.expr);
    this.left = l;
    this.right = r;
  }
}
