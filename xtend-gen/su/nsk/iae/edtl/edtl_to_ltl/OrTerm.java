package su.nsk.iae.edtl.edtl_to_ltl;

@SuppressWarnings("all")
public class OrTerm extends Term {
  public Term left;
  
  public Term right;
  
  public OrTerm(final Term l, final Term r) {
    super(l.expr);
    this.left = l;
    this.right = r;
  }
}
