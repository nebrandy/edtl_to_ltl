package su.nsk.iae.edtl.edtl_to_ltl;

@SuppressWarnings("all")
public class FTerm extends Term {
  public Term left;
  
  public FTerm(final Term l) {
    super(l.expr);
    this.left = l;
  }
}
