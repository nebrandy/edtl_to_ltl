package su.nsk.iae.edtl.edtl_to_ltl;

@SuppressWarnings("all")
public class WTerm extends Term {
  public Term expr;
  
  public WTerm(final Term e) {
    super(e.expr);
    this.expr = e;
  }
}
