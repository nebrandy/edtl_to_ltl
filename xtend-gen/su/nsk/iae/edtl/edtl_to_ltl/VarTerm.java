package su.nsk.iae.edtl.edtl_to_ltl;

import su.nsk.iae.edtl.edtl.PrimaryExpression;

@SuppressWarnings("all")
public class VarTerm extends Term {
  public PrimaryExpression expr;
  
  public VarTerm(final PrimaryExpression e) {
    super(e);
    this.expr = e;
  }
}
