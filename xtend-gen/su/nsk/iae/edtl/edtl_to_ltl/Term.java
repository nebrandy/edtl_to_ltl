package su.nsk.iae.edtl.edtl_to_ltl;

import su.nsk.iae.edtl.edtl.Expression;

@SuppressWarnings("all")
public class Term {
  public Expression expr;
  
  public Term left;
  
  public Term right;
  
  public Term(final Expression e) {
    this.expr = e;
  }
}
