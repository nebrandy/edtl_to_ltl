package su.nsk.iae.edtl.edtl_to_ltl;

@SuppressWarnings("all")
public class GTerm extends Term {
  public Term left;
  
  public GTerm(final Term l) {
    super(l.expr);
    this.left = l;
  }
}
