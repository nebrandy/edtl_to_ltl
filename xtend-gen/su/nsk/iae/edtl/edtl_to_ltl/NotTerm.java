package su.nsk.iae.edtl.edtl_to_ltl;

@SuppressWarnings("all")
public class NotTerm extends Term {
  public Term expr;
  
  public NotTerm(final Term e) {
    super(e.expr);
    this.expr = e;
  }
}
