package su.nsk.iae.edtl.edtl_to_ltl;

import su.nsk.iae.edtl.edtl.PrimaryExpression;

@SuppressWarnings("all")
public class BoolTerm extends Term {
  public boolean expr;
  
  public BoolTerm(final PrimaryExpression e) {
    super(e);
    this.expr = "TRUE".equals(e.getConstant());
  }
  
  public BoolTerm(final PrimaryExpression e, final boolean b) {
    super(e);
    this.expr = b;
  }
}
