package su.nsk.iae.edtl.edtl_to_ltl;

import com.google.common.base.Objects;
import com.google.common.collect.Iterables;
import com.opencsv.CSVWriter;
import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.xtext.generator.IFileSystemAccess2;
import org.eclipse.xtext.generator.IGeneratorContext;
import org.eclipse.xtext.xbase.lib.CollectionLiterals;
import org.eclipse.xtext.xbase.lib.Conversions;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.IteratorExtensions;
import su.nsk.iae.edtl.edtl.AndExpression;
import su.nsk.iae.edtl.edtl.Expression;
import su.nsk.iae.edtl.edtl.Model;
import su.nsk.iae.edtl.edtl.PrimaryExpression;
import su.nsk.iae.edtl.edtl.Requirement;
import su.nsk.iae.edtl.edtl.UnExpression;
import su.nsk.iae.edtl.generator.IEdtlGenerator;

@SuppressWarnings("all")
public class LtlGenerator implements IEdtlGenerator {
  private static final String filepath = "/Users/diskursmonger/edtl_project/su.nsk.iae.edtl.edtl_to_ltl/output.csv";
  
  private final ArrayList<String> headerCsv = CollectionLiterals.<String>newArrayList(" ", "req name", "trigger", "invariant", "final", "delay", "reaction", "release", "LTL formula");
  
  @Override
  public void beforeGenerate(final Resource input, final IFileSystemAccess2 fsa, final IGeneratorContext context) {
    try {
      File file = new File(LtlGenerator.filepath);
      FileWriter outCsv = new FileWriter(file);
      CSVWriter csvWriter = new CSVWriter(outCsv);
      csvWriter.writeNext(((String[])Conversions.unwrapArray(this.headerCsv, String.class)));
      final Model ast = ((Model[])Conversions.unwrapArray((Iterables.<Model>filter(IteratorExtensions.<EObject>toIterable(input.getAllContents()), Model.class)), Model.class))[0];
      final EList<Requirement> reqs = ast.getReqs();
      int reqNum = 1;
      for (final Requirement req : reqs) {
        {
          Term trigger = null;
          Term invariant = null;
          Term fin = null;
          Term delay = null;
          Term reaction = null;
          Term release = null;
          Expression _trigExpr = req.getTrigExpr();
          boolean _tripleEquals = (_trigExpr == null);
          if (_tripleEquals) {
            BoolTerm _boolTerm = new BoolTerm(null, true);
            trigger = _boolTerm;
          } else {
            Expression _trigExpr_1 = req.getTrigExpr();
            Term _term = new Term(_trigExpr_1);
            trigger = _term;
          }
          Expression _invExpr = req.getInvExpr();
          boolean _tripleEquals_1 = (_invExpr == null);
          if (_tripleEquals_1) {
            BoolTerm _boolTerm_1 = new BoolTerm(null, true);
            invariant = _boolTerm_1;
          } else {
            Expression _invExpr_1 = req.getInvExpr();
            Term _term_1 = new Term(_invExpr_1);
            invariant = _term_1;
          }
          Expression _finalExpr = req.getFinalExpr();
          boolean _tripleEquals_2 = (_finalExpr == null);
          if (_tripleEquals_2) {
            BoolTerm _boolTerm_2 = new BoolTerm(null, false);
            fin = _boolTerm_2;
          } else {
            Expression _finalExpr_1 = req.getFinalExpr();
            Term _term_2 = new Term(_finalExpr_1);
            fin = _term_2;
          }
          Expression _delayExpr = req.getDelayExpr();
          boolean _tripleEquals_3 = (_delayExpr == null);
          if (_tripleEquals_3) {
            BoolTerm _boolTerm_3 = new BoolTerm(null, false);
            delay = _boolTerm_3;
          } else {
            Expression _delayExpr_1 = req.getDelayExpr();
            Term _term_3 = new Term(_delayExpr_1);
            delay = _term_3;
          }
          Expression _reacExpr = req.getReacExpr();
          boolean _tripleEquals_4 = (_reacExpr == null);
          if (_tripleEquals_4) {
            BoolTerm _boolTerm_4 = new BoolTerm(null, true);
            reaction = _boolTerm_4;
          } else {
            Expression _reacExpr_1 = req.getReacExpr();
            Term _term_4 = new Term(_reacExpr_1);
            reaction = _term_4;
          }
          Expression _relExpr = req.getRelExpr();
          boolean _tripleEquals_5 = (_relExpr == null);
          if (_tripleEquals_5) {
            BoolTerm _boolTerm_5 = new BoolTerm(null, false);
            release = _boolTerm_5;
          } else {
            Expression _relExpr_1 = req.getRelExpr();
            Term _term_5 = new Term(_relExpr_1);
            release = _term_5;
          }
          final Term x0 = this.con(trigger, this.no(release));
          final Term x1 = this.con(invariant, reaction);
          final Term x2 = this.dis(release, x1);
          final Term x3 = this.con(invariant, this.no(delay));
          final Term x4 = this.until(x3, x2);
          final Term x5 = this.con(fin, x4);
          final Term x6 = this.dis(release, x5);
          final Term x7 = this.con(invariant, this.no(fin));
          final Term x8 = this.until(x7, x6);
          final Term x9 = this.impl(x0, x8);
          final Term ltl_formula = this.globally(x9);
          String _name = req.getName();
          String _plus = ("\nRequirement " + _name);
          String _plus_1 = (_plus + ":\n");
          String _plus_2 = (_plus_1 + 
            "trigger: ");
          String _convertExprToString = this.convertExprToString(req.getTrigExpr());
          String _plus_3 = (_plus_2 + _convertExprToString);
          String _plus_4 = (_plus_3 + "\n");
          String _plus_5 = (_plus_4 + 
            "invariant: ");
          String _convertExprToString_1 = this.convertExprToString(req.getInvExpr());
          String _plus_6 = (_plus_5 + _convertExprToString_1);
          String _plus_7 = (_plus_6 + "\n");
          String _plus_8 = (_plus_7 + 
            "final: ");
          String _convertExprToString_2 = this.convertExprToString(req.getFinalExpr());
          String _plus_9 = (_plus_8 + _convertExprToString_2);
          String _plus_10 = (_plus_9 + "\n");
          String _plus_11 = (_plus_10 + 
            "delay: ");
          String _convertExprToString_3 = this.convertExprToString(req.getDelayExpr());
          String _plus_12 = (_plus_11 + _convertExprToString_3);
          String _plus_13 = (_plus_12 + "\n");
          String _plus_14 = (_plus_13 + 
            "reaction: ");
          String _convertExprToString_4 = this.convertExprToString(req.getReacExpr());
          String _plus_15 = (_plus_14 + _convertExprToString_4);
          String _plus_16 = (_plus_15 + "\n");
          String _plus_17 = (_plus_16 + 
            "release: ");
          String _convertExprToString_5 = this.convertExprToString(req.getRelExpr());
          String _plus_18 = (_plus_17 + _convertExprToString_5);
          String _plus_19 = (_plus_18 + "\n\n");
          String _plus_20 = (_plus_19 + 
            "LTL formula:\n");
          String _convertTermToString = this.convertTermToString(ltl_formula);
          String _plus_21 = (_plus_20 + _convertTermToString);
          String out = (_plus_21 + "\n");
          System.out.println(out);
          final ArrayList<String> csvRow = CollectionLiterals.<String>newArrayList(Integer.valueOf(reqNum).toString(), req.getName(), 
            this.convertExprToString(req.getTrigExpr()), 
            this.convertExprToString(req.getInvExpr()), 
            this.convertExprToString(req.getFinalExpr()), 
            this.convertExprToString(req.getDelayExpr()), 
            this.convertExprToString(req.getReacExpr()), 
            this.convertExprToString(req.getRelExpr()), 
            this.convertTermToString(ltl_formula));
          csvWriter.writeNext(((String[])Conversions.unwrapArray(csvRow, String.class)));
          reqNum++;
        }
      }
      csvWriter.close();
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
  
  private String convertAttrToString(final Term attr, final String str) {
    if ((attr instanceof BoolTerm)) {
      return Boolean.valueOf(((BoolTerm)attr).expr).toString();
    } else {
      Expression attr_expr = attr.expr;
      if ((attr_expr instanceof PrimaryExpression)) {
        boolean _equals = "FALSE".equals(((PrimaryExpression)attr_expr).getConstant());
        if (_equals) {
          return "false";
        }
        boolean _equals_1 = "TRUE".equals(((PrimaryExpression)attr_expr).getConstant());
        if (_equals_1) {
          return "true";
        }
      }
      return str;
    }
  }
  
  private String convertTermToString(final Term term) {
    if ((term instanceof BoolTerm)) {
      return String.valueOf(((BoolTerm)term).expr);
    }
    if ((term instanceof VarTerm)) {
      return ((VarTerm)term).expr.getV().getName();
    }
    if ((term instanceof NotTerm)) {
      String _convertTermToString = this.convertTermToString(((NotTerm)term).expr);
      return ("¬" + _convertTermToString);
    }
    if ((term instanceof AndTerm)) {
      String _convertTermToString_1 = this.convertTermToString(((AndTerm)term).left);
      String _plus = ("(" + _convertTermToString_1);
      String _plus_1 = (_plus + " ∧ ");
      String _convertTermToString_2 = this.convertTermToString(((AndTerm)term).right);
      String _plus_2 = (_plus_1 + _convertTermToString_2);
      return (_plus_2 + ")");
    }
    if ((term instanceof OrTerm)) {
      String _convertTermToString_3 = this.convertTermToString(((OrTerm)term).left);
      String _plus_3 = ("(" + _convertTermToString_3);
      String _plus_4 = (_plus_3 + " ∨ ");
      String _convertTermToString_4 = this.convertTermToString(((OrTerm)term).right);
      String _plus_5 = (_plus_4 + _convertTermToString_4);
      return (_plus_5 + ")");
    }
    if ((term instanceof ImplTerm)) {
      String _convertTermToString_5 = this.convertTermToString(((ImplTerm)term).left);
      String _plus_6 = ("(" + _convertTermToString_5);
      String _plus_7 = (_plus_6 + " → ");
      String _convertTermToString_6 = this.convertTermToString(((ImplTerm)term).right);
      String _plus_8 = (_plus_7 + _convertTermToString_6);
      return (_plus_8 + ")");
    }
    if ((term instanceof WTerm)) {
      String _convertTermToString_7 = this.convertTermToString(((WTerm)term).expr);
      String _plus_9 = ("W(" + _convertTermToString_7);
      return (_plus_9 + ")");
    }
    if ((term instanceof FTerm)) {
      String _convertTermToString_8 = this.convertTermToString(((FTerm)term).left);
      String _plus_10 = ("F(" + _convertTermToString_8);
      return (_plus_10 + ")");
    }
    if ((term instanceof GTerm)) {
      String _convertTermToString_9 = this.convertTermToString(((GTerm)term).left);
      String _plus_11 = ("G(" + _convertTermToString_9);
      return (_plus_11 + ")");
    }
    if ((term instanceof UTerm)) {
      String _convertTermToString_10 = this.convertTermToString(((UTerm)term).left);
      String _plus_12 = ("(" + _convertTermToString_10);
      String _plus_13 = (_plus_12 + " U ");
      String _convertTermToString_11 = this.convertTermToString(((UTerm)term).right);
      String _plus_14 = (_plus_13 + _convertTermToString_11);
      return (_plus_14 + ")");
    }
    return this.convertExprToString(term.expr);
  }
  
  private String convertExprToString(final Expression expr) {
    if ((expr instanceof PrimaryExpression)) {
      boolean _equals = "FALSE".equals(((PrimaryExpression)expr).getConstant());
      if (_equals) {
        return "false";
      }
      boolean _equals_1 = "TRUE".equals(((PrimaryExpression)expr).getConstant());
      if (_equals_1) {
        return "true";
      }
      return ((PrimaryExpression)expr).getV().getName();
    }
    if ((expr instanceof AndExpression)) {
      String _convertExprToString = this.convertExprToString(((AndExpression)expr).getLeft());
      String _plus = ("(" + _convertExprToString);
      String _plus_1 = (_plus + " ∧ ");
      String _convertExprToString_1 = this.convertExprToString(((AndExpression)expr).getRight());
      String _plus_2 = (_plus_1 + _convertExprToString_1);
      return (_plus_2 + ")");
    }
    if ((expr instanceof UnExpression)) {
      String _unOp = ((UnExpression)expr).getUnOp();
      boolean _tripleEquals = (_unOp == "NOT");
      if (_tripleEquals) {
        String _convertExprToString_2 = this.convertExprToString(((UnExpression)expr).getRight());
        return ("¬" + _convertExprToString_2);
      }
      String _unOp_1 = ((UnExpression)expr).getUnOp();
      boolean _tripleEquals_1 = (_unOp_1 == "FE");
      if (_tripleEquals_1) {
        String _convertExprToString_3 = this.convertExprToString(((UnExpression)expr).getRight());
        String _plus_3 = ("FE (" + _convertExprToString_3);
        return (_plus_3 + ")");
      }
      String _unOp_2 = ((UnExpression)expr).getUnOp();
      boolean _tripleEquals_2 = (_unOp_2 == "RE");
      if (_tripleEquals_2) {
        String _convertExprToString_4 = this.convertExprToString(((UnExpression)expr).getRight());
        String _plus_4 = ("RE (" + _convertExprToString_4);
        return (_plus_4 + ")");
      }
      String _unOp_3 = ((UnExpression)expr).getUnOp();
      boolean _tripleEquals_3 = (_unOp_3 == "HIGH");
      if (_tripleEquals_3) {
        String _convertExprToString_5 = this.convertExprToString(((UnExpression)expr).getRight());
        String _plus_5 = ("HIGH (" + _convertExprToString_5);
        return (_plus_5 + ")");
      }
      String _unOp_4 = ((UnExpression)expr).getUnOp();
      boolean _tripleEquals_4 = (_unOp_4 == "LOW");
      if (_tripleEquals_4) {
        String _convertExprToString_6 = this.convertExprToString(((UnExpression)expr).getRight());
        String _plus_6 = ("LOW (" + _convertExprToString_6);
        return (_plus_6 + ")");
      }
    }
    String _orOp = expr.getOrOp();
    boolean _tripleNotEquals = (_orOp != null);
    if (_tripleNotEquals) {
      String _convertExprToString_7 = this.convertExprToString(expr.getLeft());
      String _plus_7 = ("(" + _convertExprToString_7);
      String _plus_8 = (_plus_7 + " ∨ ");
      String _convertExprToString_8 = this.convertExprToString(expr.getRight());
      String _plus_9 = (_plus_8 + _convertExprToString_8);
      return (_plus_9 + ")");
    }
    return null;
  }
  
  private Term con(final Term left, final Term right) {
    Expression left_expr = left.expr;
    if ((left_expr instanceof PrimaryExpression)) {
      boolean _equals = "FALSE".equals(((PrimaryExpression)left_expr).getConstant());
      if (_equals) {
        return new BoolTerm(((PrimaryExpression)left_expr));
      }
    }
    Expression right_expr = right.expr;
    if ((right_expr instanceof PrimaryExpression)) {
      boolean _equals_1 = "FALSE".equals(((PrimaryExpression)right_expr).getConstant());
      if (_equals_1) {
        return new BoolTerm(((PrimaryExpression)right_expr));
      }
    }
    if ((left_expr instanceof PrimaryExpression)) {
      boolean _equals_2 = "TRUE".equals(((PrimaryExpression)left_expr).getConstant());
      if (_equals_2) {
        return right;
      }
    }
    if ((right_expr instanceof PrimaryExpression)) {
      boolean _equals_3 = "TRUE".equals(((PrimaryExpression)right_expr).getConstant());
      if (_equals_3) {
        return left;
      }
    }
    if ((left instanceof BoolTerm)) {
      if ((!((BoolTerm)left).expr)) {
        return left;
      } else {
        return right;
      }
    }
    if ((right instanceof BoolTerm)) {
      if ((!((BoolTerm)right).expr)) {
        return right;
      } else {
        return left;
      }
    }
    return new AndTerm(left, right);
  }
  
  private Term dis(final Term left, final Term right) {
    if ((left instanceof BoolTerm)) {
      if (((BoolTerm)left).expr) {
        return left;
      } else {
        return right;
      }
    }
    if ((right instanceof BoolTerm)) {
      if (((BoolTerm)right).expr) {
        return right;
      } else {
        return left;
      }
    }
    Expression left_expr = left.expr;
    if ((left_expr instanceof PrimaryExpression)) {
      boolean _equals = "TRUE".equals(((PrimaryExpression)left_expr).getConstant());
      if (_equals) {
        return new BoolTerm(((PrimaryExpression)left_expr));
      }
    }
    Expression right_expr = right.expr;
    if ((right_expr instanceof PrimaryExpression)) {
      boolean _equals_1 = "TRUE".equals(((PrimaryExpression)right_expr).getConstant());
      if (_equals_1) {
        return new BoolTerm(((PrimaryExpression)right_expr));
      }
    }
    if ((left_expr instanceof PrimaryExpression)) {
      boolean _equals_2 = "FALSE".equals(((PrimaryExpression)left_expr).getConstant());
      if (_equals_2) {
        return right;
      }
    }
    if ((right_expr instanceof PrimaryExpression)) {
      boolean _equals_3 = "FALSE".equals(((PrimaryExpression)right_expr).getConstant());
      if (_equals_3) {
        return left;
      }
    }
    boolean _equals_4 = left.equals(right);
    if (_equals_4) {
      return left;
    }
    if ((right instanceof AndTerm)) {
      Term right_r = ((AndTerm)right).right;
      if ((right_r instanceof OrTerm)) {
        boolean _equals_5 = ((OrTerm)right_r).left.equals(left);
        if (_equals_5) {
          AndTerm _andTerm = new AndTerm(((AndTerm)right).left, ((OrTerm)right_r).right);
          return new OrTerm(left, _andTerm);
        }
      }
    }
    if ((right instanceof FTerm)) {
      Term right_l = ((FTerm)right).left;
      if ((right_l instanceof OrTerm)) {
        boolean _equals_6 = left.equals(((OrTerm)right_l).left);
        if (_equals_6) {
          return right;
        }
      }
    }
    if ((right instanceof UTerm)) {
      Term right_r_1 = ((UTerm)right).right;
      boolean _equals_7 = right_r_1.equals(left);
      if (_equals_7) {
        return right;
      }
    }
    if ((left instanceof GTerm)) {
      Term left_l = ((GTerm)left).left;
      if ((left_l instanceof NotTerm)) {
        if ((right instanceof FTerm)) {
          boolean _equals_8 = ((NotTerm)left_l).expr.equals(((FTerm)right).left.expr);
          if (_equals_8) {
            return new BoolTerm(null, true);
          }
        }
      }
    }
    if ((right instanceof FTerm)) {
      boolean _equals_9 = ((FTerm)right).left.equals(left);
      if (_equals_9) {
        return right;
      }
    }
    if ((right instanceof OrTerm)) {
      if ((((OrTerm)right).left.equals(left) || ((OrTerm)right).right.equals(left))) {
        return right;
      }
    }
    if ((left instanceof GTerm)) {
      if ((((GTerm)left).left instanceof NotTerm)) {
        if ((right instanceof OrTerm)) {
          if ((((OrTerm)right).left instanceof FTerm)) {
            boolean _equals_10 = ((OrTerm)right).left.expr.equals(((GTerm)left).left.expr);
            if (_equals_10) {
              return ((OrTerm)right).right;
            }
          }
        }
      }
    }
    if ((right instanceof UTerm)) {
      Term right_r_2 = ((UTerm)right).right;
      if ((right_r_2 instanceof OrTerm)) {
        boolean _equals_11 = ((OrTerm)right_r_2).left.equals(left);
        if (_equals_11) {
          return right;
        }
      }
    }
    return new OrTerm(left, right);
  }
  
  private boolean checkNotHelper(final Term left, final Term right) {
    if (((left instanceof BoolTerm) && (right instanceof BoolTerm))) {
      return (!Objects.equal(left.expr, right.expr));
    }
    if (((left instanceof NotTerm) && (!(right instanceof NotTerm)))) {
      return left.expr.equals(right.expr);
    }
    if (((!(left instanceof NotTerm)) && (right instanceof NotTerm))) {
      return right.expr.equals(left.expr);
    }
    return false;
  }
  
  private Term until(final Term left, final Term right) {
    if ((right instanceof BoolTerm)) {
      if (((BoolTerm)right).expr) {
        return new BoolTerm(null, true);
      } else {
        return new BoolTerm(null, false);
      }
    }
    if ((left instanceof BoolTerm)) {
      if (((BoolTerm)left).expr) {
        if ((!(right instanceof BoolTerm))) {
          return this.future(right);
        }
      } else {
        if ((!(right instanceof BoolTerm))) {
          return right;
        }
      }
    }
    boolean _equals = left.equals(right);
    if (_equals) {
      return left;
    }
    boolean _checkNotHelper = this.checkNotHelper(left, right);
    if (_checkNotHelper) {
      return this.future(right);
    }
    if ((left instanceof NotTerm)) {
      if ((right instanceof OrTerm)) {
        boolean _checkNotHelper_1 = this.checkNotHelper(left, ((OrTerm)right).right);
        if (_checkNotHelper_1) {
          return this.dis(this.future(((NotTerm)left).expr), this.until(left, ((OrTerm)right).left));
        }
      }
    }
    if ((left instanceof AndTerm)) {
      if ((right instanceof OrTerm)) {
        boolean _equals_1 = ((AndTerm)left).left.equals(((OrTerm)right).right);
        if (_equals_1) {
          UTerm _uTerm = new UTerm(left, ((OrTerm)right).left);
          return this.dis(((AndTerm)left).left, _uTerm);
        }
      }
    }
    if ((left instanceof AndTerm)) {
      boolean _equals_2 = ((AndTerm)left).left.equals(right);
      if (_equals_2) {
        return right;
      }
    }
    if ((right instanceof OrTerm)) {
      boolean _equals_3 = left.equals(((OrTerm)right).right);
      if (_equals_3) {
        return this.dis(left, this.until(left, ((OrTerm)right).left));
      }
    }
    if ((left instanceof AndTerm)) {
      if ((right instanceof AndTerm)) {
        boolean _equals_4 = ((AndTerm)left).left.equals(((AndTerm)right).right);
        if (_equals_4) {
          boolean _checkNotHelper_2 = this.checkNotHelper(((AndTerm)left).right, ((AndTerm)right).left);
          if (_checkNotHelper_2) {
            return this.until(((AndTerm)left).left, right);
          }
        }
      }
    }
    return new UTerm(left, right);
  }
  
  private Term impl(final Term left, final Term right) {
    if (((left instanceof BoolTerm) || (right instanceof BoolTerm))) {
      return this.dis(this.no(left), right);
    } else {
      return new ImplTerm(left, right);
    }
  }
  
  private Term future(final Term t) {
    if ((t instanceof BoolTerm)) {
      return t;
    } else {
      return new FTerm(t);
    }
  }
  
  private Term no(final Term e) {
    if ((e instanceof NotTerm)) {
      return ((NotTerm)e).expr;
    }
    if ((e instanceof BoolTerm)) {
      ((BoolTerm)e).expr = (!((BoolTerm)e).expr);
      return e;
    }
    return new NotTerm(e);
  }
  
  private Term globally(final Term t) {
    if ((t instanceof BoolTerm)) {
      return t;
    }
    if ((t instanceof FTerm)) {
      return new GTerm(t);
    }
    if ((t instanceof GTerm)) {
      return new GTerm(((GTerm)t).left);
    }
    if ((t instanceof OrTerm)) {
      if ((((OrTerm)t).left instanceof GTerm)) {
        if ((((OrTerm)t).right instanceof UTerm)) {
          if ((((OrTerm)t).right.right instanceof AndTerm)) {
            if ((((OrTerm)t).left.left instanceof AndTerm)) {
              boolean _equals = ((OrTerm)t).left.left.left.left.equals(((OrTerm)t).right.left);
              if (_equals) {
                boolean _equals_1 = ((OrTerm)t).right.left.equals(((OrTerm)t).right.right.right);
                if (_equals_1) {
                  boolean _checkNotHelper = this.checkNotHelper(((OrTerm)t).left.left.right, ((OrTerm)t).right.right.left);
                  if (_checkNotHelper) {
                    return new GTerm(((OrTerm)t).left.left.left);
                  }
                }
              }
            }
          }
        }
      }
    }
    if ((t instanceof OrTerm)) {
      if ((((OrTerm)t).left instanceof GTerm)) {
        if ((((OrTerm)t).right instanceof UTerm)) {
          if ((((OrTerm)t).right.right instanceof AndTerm)) {
            if ((((OrTerm)t).right.right.right instanceof UTerm)) {
              if ((((OrTerm)t).right.right.right.right instanceof AndTerm)) {
                boolean _equals_2 = ((OrTerm)t).left.left.equals(((OrTerm)t).right.left);
                if (_equals_2) {
                  boolean _equals_3 = ((OrTerm)t).left.left.left.equals(((OrTerm)t).right.right.right.left);
                  if (_equals_3) {
                    boolean _equals_4 = ((OrTerm)t).left.left.left.equals(((OrTerm)t).right.right.right.right.left);
                    if (_equals_4) {
                      boolean _checkNotHelper_1 = this.checkNotHelper(((OrTerm)t).left.left.right, ((OrTerm)t).right.right.left);
                      if (_checkNotHelper_1) {
                        return this.globally(this.con(((OrTerm)t).left.left.left, this.dis(this.globally(((OrTerm)t).left.left.right), 
                          this.future(this.con(((OrTerm)t).right.right.left, this.future(((OrTerm)t).right.right.right.right.right))))));
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    if ((t instanceof OrTerm)) {
      if ((((OrTerm)t).right instanceof UTerm)) {
        if ((((OrTerm)t).left instanceof GTerm)) {
          if ((((OrTerm)t).left.left instanceof AndTerm)) {
            if ((((OrTerm)t).right.left instanceof AndTerm)) {
              boolean _equals_5 = ((OrTerm)t).left.left.equals(((OrTerm)t).right.left);
              if (_equals_5) {
                if ((((OrTerm)t).right.right instanceof AndTerm)) {
                  if ((((OrTerm)t).right.right.right instanceof AndTerm)) {
                    boolean _checkNotHelper_2 = this.checkNotHelper(((OrTerm)t).left.left.right, ((OrTerm)t).right.right.left);
                    if (_checkNotHelper_2) {
                      return this.globally(this.con(((OrTerm)t).left.left.left, this.dis(this.globally(((OrTerm)t).left.left.right), 
                        this.future(this.con(((OrTerm)t).right.right.left, ((OrTerm)t).right.right.right.right)))));
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
    if ((t instanceof OrTerm)) {
      if ((((OrTerm)t).left instanceof GTerm)) {
        if ((((OrTerm)t).right instanceof UTerm)) {
          boolean _equals_6 = ((OrTerm)t).left.left.equals(((OrTerm)t).right.left);
          if (_equals_6) {
            return this.globally(this.con(((OrTerm)t).left.left, this.until(((OrTerm)t).left.right, ((OrTerm)t).right.right)));
          }
        }
      }
    }
    return new GTerm(t);
  }
  
  @Override
  public void setModel(final Model model) {
  }
  
  @Override
  public void doGenerate(final Resource input, final IFileSystemAccess2 fsa, final IGeneratorContext context) {
  }
  
  @Override
  public void afterGenerate(final Resource input, final IFileSystemAccess2 fsa, final IGeneratorContext context) {
  }
}
